var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(document).ready(function () {
    $('.select2-general-nosearch').select2({ minimumResultsForSearch: Infinity });

    $('.select2-general-ajax-static').select2({
        ajax: {
            url: function () {
                return $(this).attr('data-show');
            },
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                    devicetype: $(this).attr('data-device'),
                    firstreff: $(this).attr('data-reff') != '' ? $('#' + $(this).attr('data-reff')).val() : ''
                };
            },
            processResults: function (response) {
                return {
                    results: response
                }
            },
            cache: true
        }
    })

    $('.select2-general-ajax').select2({
        tags: true,
        ajax: {
            url: function () {
                return $(this).attr('data-show');
            },
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term,
                    devicetype: $(this).attr('data-device'),
                    datareference: $(this).attr("data-reff") != '' ? $('#' + $(this).attr("data-reff")).val() : '',
                };
            },
            processResults: function (response) {
                return {
                    results: response
                }
            },
            cache: true
        }
    });

    $('.select2-optiondynamic').on('select2:select', function (e) {
        var devicetype = $(this).attr("data-device");
        var storeurl = $(this).attr("data-store");
        var formid = $(this).attr("data-form");
        var changeOption = $(this).attr("data-change");
        var newoption = e.params.data.text;

        var data = {
            _token: CSRF_TOKEN,
            devicetype: devicetype != '' ? devicetype : '',
            newoption: newoption,
            datareference: $(this).attr("data-reff") != '' ? $('#' + $(this).attr("data-reff")).val() : '',
        }

        $.ajax({
            url: storeurl,
            method: "POST",
            data: data,
            dataType: "json",
            success: function (response) {
                if (changeOption == "true") {
                    $('#' + formid).val(null).trigger('change');

                    var option = new Option(response.message.text, response.message.id, true, true);
                    $('#' + formid).append(option).trigger('change');

                    $('#' + formid).trigger('change');
                }
            },
            error: function (response) {
                if (response.status === 422) {
                    let errArr = "";
                    $.each(response.responseJSON.errors, function(index, value) {
                        errArr = errArr + "<li>" + value + "</li>";
                    });

                    $('.warnings').show().html(
                        "<div class=\"alert alert-warning\">" +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
                        "<span aria-hidden=\"true\">&times;</span> </button>" +
                        "<h3 class=\"text-warning\"><i class=\"fa fa-exclamation-circle\"></i> Warning</h3>" +
                        "<ul>" +
                        errArr +
                        "</ul>" +
                        "</div>");
                    
                    $('#' + formid).val(null).trigger('change');
                } else {
                    toastr.error(response.responseJSON.errors);
                    $('#' + formid).val('').trigger('change');
                }
            }
        });
    });

    $('#businessunit').on('change', function (e) {
        $('#platform').val('').trigger('change');
    });
});

