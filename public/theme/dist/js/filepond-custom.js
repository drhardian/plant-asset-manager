FilePond.registerPlugin(
    FilePondPluginFileValidateType,
    FilePondPluginFileValidateSize,
    FilePondPluginImageExifOrientation,
    FilePondPluginImagePreview,
    FilePondPluginImageCrop,
    FilePondPluginImageResize,
    FilePondPluginImageTransform,
);

FilePond.create(
    document.querySelector('input[id="imgtoupload"'), {
        labelIdle: `Drag & Drop your picture or <span class="filepond--label-action">Browse</span>`,
        imagePreviewHeight: 170,
        imageCropAspectRatio: '1:1',
        imageResizeTargetWidth: 200,
        imageResizeTargetHeight: 200,
        maxFileSize: '2MB',
        stylePanelLayout: 'compact circle',
        styleLoadIndicatorPosition: 'center bottom',
        styleProgressIndicatorPosition: 'right bottom',
        styleButtonRemoveItemPosition: 'left bottom',
        styleButtonProcessItemPosition: 'right bottom',
        maxFiles: 1,
        allowPaste: false,
        acceptedFileTypes: ['image/png', 'image/jpeg', 'image/jpg'],
    }
);

FilePond.setOptions({
    server: {
        process: {
            url: $('#imgid').attr('data-imgupload'),
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': CSRF_TOKEN
            },
            onload: function (response) { 
                $('#imgid').val(response);
                return response; 
            }
        },
        revert: (uniqueFileId, load, abort) => {
            $.ajax({
                url: $('#imgid').attr('data-imgrevert'),
                method: "DELETE",
                data: {
                    processid: $('#imgid').attr('data-processid'),
                    foldername: $('#imgid').val(),
                    tagnum_initial: $('#tagnum_initial').val()
                },
                headers: {
                    'X-CSRF-TOKEN': CSRF_TOKEN,
                    '_method': 'DELETE'
                },
                success: function (response) {
                    load();
                },
                error: function (xhr) {
                    abort();
                }
            });
        },
        restore: (src, load, error, progress, abort) => {
            if(!$('#imgid').val()) {
                abort();
            } else {
                $.ajax({
                    url: src,
                    data: {
                        foldername: $('#imgid').val(),
                        tagnum_initial: $('#tagnum_initial').val()
                    },
                    xhrFields:{
                        responseType: 'blob'
                    },
                    success: function (response) {
                        load(response);
                    },
                    error: function (xhr) {
                        abort();
                    }
                });
            }
        },
    },
    files: [
        {
            source: $('#imgid').attr('data-imgrestore'),
            options: {
                type: 'limbo'
            }
        }
    ]
});