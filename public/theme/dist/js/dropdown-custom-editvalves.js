let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
let arrField = ['tagnumber','businessunit','platform','facility','equipnum','application','prodloss','nameplate','method','valvetype'];

$(document).ready(function () {
    $('.select2-general-nosearch').select2({minimumResultsForSearch: Infinity});

    $('.select2-general-ajax-static').select2({
        ajax: {
            url: function() {
                return $(this).attr('data-show');
            },
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                if($(this).attr('data-reff') === '') {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        devicetype: ''
                    };
                } else {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        devicetype: $(this).attr('data-device'),
                        datareference: $('#' + $(this).attr('data-reff')).val()
                    };
                }
            },
            processResults: function(response) {
                return {
                    results: response
                }
            },
            cache: true
        }
    });

    $('.select2-general-ajax').select2({
        tags: true,
        ajax: {
            url: function() {
                return $(this).attr('data-show');
            },
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                    devicetype: $(this).attr('data-device'),
                    datareference: $(this).attr("data-reff") != '' ? $('#' + $(this).attr("data-reff")).val() : ''
                };
            },
            processResults: function(response) {
                return {
                    results: response
                }
            },
            cache: true
        }
    });

    $('.select2-optiondynamic').on('select2:select', function(e) {
        var devicetype = $(this).attr("data-device");
        var storeurl = $(this).attr("data-store");
        var formid = $(this).attr("data-form");
        var changeOption = $(this).attr("data-change");
        var newoption = e.params.data.text;

        var data = {
            _token: CSRF_TOKEN,
            devicetype: devicetype != '' ? devicetype : '',
            newoption: newoption,
            datareference: $(this).attr("data-reff") != '' ? $('#' + $(this).attr("data-reff")).val() : ''
        }

        $.ajax({
            url: storeurl,
            method: "POST",
            data: data,
            dataType: "json",
            success: function(response) {
                if(changeOption == "true") {
                    $('#'+formid).val(null).trigger('change');

                    var option = new Option(response.message.text, response.message.id, true, true);
                    $('#'+formid).append(option).trigger('change');

                    $('#'+formid).trigger('change');
                }
            },
            error: function(response) {
                console.log(response.status);
            }
        });
    });

    $('#businessunit').on('change', function (e) {
        $('#platform').val('').trigger('change');
    });

    if($('#tagnum_initial').val() != '') {
        $.each(arrField, function (index, value) { 
            if(value != "tagnumber" && value != "method") {
                $('#'+value).val(null).trigger('change');
                var option = new Option($('#'+value).attr('data-text'), $('#'+value).attr('data-id'), true, true);
            }

            $('#'+value).append(option).trigger('change');
            $('#'+value).trigger('change');
        });

        const deviceType = $("[name='devicetype']").val();

        switch ( deviceType ) {
            case 'AOF':
                var arrValveInfo = ['bodymfr','bodyserialnum','bodymodel','bodysize','bodymaterial','classrating','endconn',
                                'materialplug','materialseat','materialstem','leakageclass','actumfr','actumodel','actuserialnum',
                                'actusize','ratioinfo','failpos','gearmfr','gearmodel','gearsize','instacc','instserialnum'];                
                break;
            case 'COV':
                var arrValveInfo = ['bodymfr','bodyserialnum','bodymodel','bodysize','bodymaterial','classrating','endconn',
                                'leakageclass','flowdirect','actumfr','actuserialnum','actumodel','actusize','failpos',
                                'gearmfr','gearmodel','gearsize','postmfr','postserialnum','postmodel','commprot','instacc','instserialnum'];
                break;
            case 'CKV':
                var arrValveInfo = ['bodymfr','bodyserialnum','bodymodel','bodysize','bodymaterial','classrating','endconn','design','seatmaterial'];
                break;
            case 'REG':
                var arrValveInfo = ['bodymfr','bodyserialnum','bodymodel','bodysize','bodymaterial','classrating','endconn','orificesize','springrange',
                        'springcolor','setpoint','pilotmfr','pilotmodel','regsize'];
                break;                            
            case 'MAV':
                var arrValveInfo = ['bodymfr','bodyserialnum','bodymodel','bodysize','bodymaterial','classrating','endconn','leakageclass','seat','stem'];
                break;
            case 'PRV':
                var arrValveInfo = ['bodymfr','bodyserialnum','bodymodel','bodymaterial','classrating','prvinlet','prvoutlet','orfcsize','prvset','prvcapacity'];
                break;        
        }

        $.each(arrValveInfo, function (index, value) {
            $('#'+value).val(null).trigger('change');

            if( $('#'+value).attr('data-text') !== '' ) {
                var option = new Option($('#'+value).attr('data-text'), $('#'+value).attr('data-id'), true, true);
            } else {
                var option = new Option( 'Select here...', '', true, true);
            }

            $('#'+value).append(option).trigger('change');
            $('#'+value).trigger('change');
        });
    }
});
