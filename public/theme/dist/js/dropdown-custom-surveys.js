var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(document).ready(function () {
    $('.select2-general-nosearch').select2({ 
        minimumResultsForSearch: Infinity 
    });

    $('.select2-general-ajax-static').select2({
        ajax: {
            url: function () {
                return $(this).attr('data-show');
            },
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var datareff = $(this).attr('data-reff');

                if (datareff === '') {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        devicetype: ''
                    };
                } else {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        devicetype: $(this).attr('data-device'),
                        firstreff: $('#' + datareff).val()
                    };
                }
            },
            processResults: function (response) {
                return {
                    results: response
                }
            },
            cache: true
        }
    });

    $('.select2-general-ajax').select2({
        tags: true,
        ajax: {
            url: function () {
                return $(this).attr('data-show');
            },
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                    devicetype: $(this).attr('data-device'),
                    comboid: $(this).attr('data-alias')
                };
            },
            processResults: function (response) {
                return {
                    results: response
                }
            },
            cache: true
        }
    });

    $('.select2-general-ajax-multiple').select2({
        allowClear: true,
        width: 'resolve',
        tags: true,
        ajax: {
            url: function () {
                return $(this).attr('data-show');
            },
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                    devicetype: $(this).attr('data-devicetype'),
                    comboid: $(this).attr('data-comboid'),
                    combotype: $(this).attr('data-combotype')
                };
            },
            processResults: function (response) {
                return {
                    results: response
                }
            },
            cache: true
        },
        placeholder: 'Search here...'
    });

    $('.select2-optiondynamic').on('select2:select', function (e) {
        var devicetype = $(this).attr("data-device");
        var storeurl = $(this).attr("data-store");
        var formid = $(this).attr("data-form");
        var changeOption = $(this).attr("data-change");
        var newoption = e.params.data.text;

        if (devicetype != '') {
            var data = {
                _token: CSRF_TOKEN,
                devicetype: devicetype,
                newoption: newoption
            }
        } else {
            var data = {
                _token: CSRF_TOKEN,
                newoption: newoption
            }
        }

        $.ajax({
            url: storeurl,
            method: "POST",
            data: data,
            dataType: "json",
            success: function (response) {
                if (changeOption == "true") {
                    $('#' + formid).val(null).trigger('change');

                    var option = new Option(response.message.text, response.message.id, true, true);
                    $('#' + formid).append(option).trigger('change');

                    $('#' + formid).trigger('change');
                }
            },
            error: function (xhr) {
                toastr.error(xhr.responseJSON.errors);
            }
        });
    });

    $('.select2-optiondynamic-one').on('select2:select', function (e) {
        var devicetype = $(this).attr("data-device");
        var storeurl = $(this).attr("data-store");
        var formid = $(this).attr("data-form");
        var formname = $(this).attr("data-name");
        var changeOption = $(this).attr("data-change"); // del
        var formalias = $(this).attr("data-alias");
        var resetOption = $(this).attr("data-reset");
        var resetReff = $(this).attr("data-reff");
        var newoption = e.params.data.text;

        if (devicetype != '') {
            var data = {
                _token: CSRF_TOKEN,
                devicetype: devicetype,
                comboid: formalias,
                formid: formid,
                resetoption: resetOption,
                conditionlevel: $('#' + resetReff).val(),
                newoption: newoption
            }
        } else {
            var data = {
                _token: CSRF_TOKEN,
                newoption: newoption
            }
        }

        $.ajax({
            url: storeurl,
            method: "POST",
            data: data,
            dataType: "json",
            success: function (response) {

                if (response.status == 400) {
                    $('#' + formname).val('').trigger('change');
                    $('.' + resetReff + '-feedback').text(response.message.conditionlevel[0]).addClass('text-danger');
                } else {
                    $('.' + resetReff + '-feedback').text('').removeClass('text-danger');

                    if (resetOption == "true") {
                        $('#' + resetReff).val('').trigger('change');

                        var option = new Option(response.message.text, response.message.id, true, true);
                        $('#' + resetReff).append(option).trigger('change');

                        $('#' + resetReff).trigger('change');
                    }
                }


                // if(changeOption == "true") {
                //     $('#'+formname).val(null).trigger('change');

                //     var option = new Option(response.message.text, response.message.id, true, true);
                //     $('#'+formname).append(option).trigger('change');

                //     $('#'+formname).trigger('change');
                // }
            },
            error: function (xhr) {
                toastr.error(xhr.responseJSON.message);
            }
        });
    });

    $('.select2-optiondynamic-two').on('select2:select', function (e) {
        var storeurl = $(this).attr("data-store");
        var devicetype = $(this).attr("data-devicetype");
        var comboid = $(this).attr("data-comboid");
        var combotype = $(this).attr("data-combotype");
        var labeldesc = $(this).attr("data-labeldesc");
        var newoption = e.params.data.text;

        var data = {
            _token: CSRF_TOKEN,
            devicetype: devicetype,
            comboid: comboid,
            combotype: combotype,
            labeldesc: labeldesc,
            newoption: newoption
        }

        $.ajax({
            url: storeurl,
            method: "POST",
            data: data,
            dataType: "json",
            error: function (xhr) {
                toastr.error(xhr.responseJSON.error);
            }
        });
    });
});
