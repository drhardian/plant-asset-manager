function loadInitialData(url) {
    let initialParameters = {
        filter: $('#filterCheckbox').is(':checked')
    };

    $('#main-table').empty();
    $('#main-table').append('<table id="details-table" class="table table-sm table-bordered nowrap" style="width:100%">'+
                            '<thead>' +
                                '<tr>' + 
                                    '<th class="text-center" style="vertical-align: middle;">Action</th>' +
                                    '<th class="text-center" style="vertical-align: middle;">Description</th>' +
                                '</tr>' +
                            '</thead>' +
                            '</table>');

    let columns = [
        { data: 'action', name: 'action', className: 'text-center', width: '10%', orderable: false },
        { data: 'description', name: 'description' }
    ];

    loadTable(url, initialParameters, columns);
}

function loadTable(url, parameters, columns) {
    $('#details-table').DataTable({
        language: {
            processing: "Loading. Please wait..."
        },
        processing: true,
        serverSide: true,
        autoWidth: true,
        deferRender: true,
        lengthMenu: [10, 20, 30],
        select: {
            style: 'single'
        },
        search: {
            return: true
        },
        ajax: {
            url: url,
            data: parameters
        },
        columns: columns,
        searchable: true,
        order: []
    });

    $('#details-table').on('select.dt', function (e, dt, type, indexes) {
        if ( type === 'row' ) {
            let id = $('#details-table').DataTable().rows( indexes ).data().pluck( 'id' );
            let type = $('#details-table').DataTable().rows(indexes).data().pluck('tableType');
            
            if (type[0] === 'item') {
                let urlSelectedItem = sessionStorage.getItem('selecteditem_url');
                openFormItem(urlSelectedItem, id[0]);
            }
        }
    });

    $('#details-table').on('deselect.dt', function (e, dt, type, indexes) {
        cbTags_clearselected();
        $('#details_info_item')[0].reset();
    });
}