function loadItemSubTable(url, parameters, columns) {
    // console.log([url, parameters, columns])

    $('#item-sub1-table').DataTable({
        language: {
            processing: "Loading. Please wait..."
        },
        processing: true,
        serverSide: true,
        autoWidth: true,
        deferRender: true,
        lengthMenu: [5],
        lengthChange: false,
        select: {
            style: 'single'
        },
        ajax: {
            url: url,
            data: parameters
        },
        columns: columns,
        searchable: true,
        order: []
    });

    $('#item-sub1-table').on('select.dt', function (e, dt, type, indexes) {
        if ( type === 'row' ) {
            let id = $('#item-sub1-table').DataTable().rows( indexes ).data().pluck( 'id' );
            let type = $('#item-sub1-table').DataTable().rows(indexes).data().pluck('tableType');
            
            if (type[0] === 'item') {
                let url = sessionStorage.getItem('selecteditem_url');
                openFormItem(url, id[0]);
            }
        }
    });

    $('#item-sub1-table').on('deselect.dt', function (e, dt, type, indexes) {

    });
}

function resetItemSubTable() {
    $('#item-sub1-table').DataTable().clear().destroy();
}