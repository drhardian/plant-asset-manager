$('#item_content').perfectScrollbar();

function showFrameItem(subTableUrl) {
    $('#details_info_item').show();

    let initialParameters = {
        id: $('#item_id').val()
    };

    let columns = [
        { data: 'action', name: 'action', className: 'text-center', width: '10%', orderable: false },
        { data: 'field_1', name: 'field_1' },
        { data: 'field_2', name: 'field_2' }
    ];

    loadItemSubTable(subTableUrl, initialParameters, columns);
}

function openFormItem(url, id) {
    $.ajax({
        type: "get",
        url: url,
        data: {
            id: id
        },
        success: function (response) {
            sessionStorage.removeItem('selected_tagRefference');
            sessionStorage.setItem('selected_tagRefference', response.tagRefference);

            cbTags_setselected(response.selectedTag.id, response.selectedTag.text);

            loadItemDetails(response.selectedTag.id);
        },
        error: function (response) {
            console.log(response);
        }
    });

    $('#details_info_item').show();
}

function loadItemDetails(id) {
    $.ajax({
        type: "get",
        url: sessionStorage.getItem('detailsitem_url'),
        data: {
            id: id
        },
        success: function (response) {
            $.each(response.item_details, function (index, value) { 
                $('#item_' + index).val(value);
                if (index !== "id") {
                    $('#item_' + index).prop('disabled', true);
                }
            });
        }
    });
}

function editFormItem() {
    $('#item_field_1').prop('disabled', false);
    $('#item_field_2').prop('disabled', false);
    $('#item_field_3').prop('disabled', false);
}
