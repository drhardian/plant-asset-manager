function cbTags_setselected(id,text) {
    var option = new Option(text, id, true, true);
    $('#tags_item').append(option).trigger('change');
}

function cbTags_clearselected() {
    $('#tags_item').val('0').trigger('change');
}

$(document).ready(function () {
    $('.select2-tags-item').select2({
        ajax: {
            url: function () {
                return $(this).attr('data-show');
            },
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term,
                    tag_refference: sessionStorage.getItem('selected_tagRefference')
                };
            },
            processResults: function (response) {
                return {
                    results: response
                }
            },
            cache: true
        }
    });

    $('.select2-tags-item').on('select2:select', function (e) {
        loadItemDetails($('#tags_item').val());
    });
});