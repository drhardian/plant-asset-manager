function loadTreeContent(url) {
    $.ajax({
        type: "get",
        url: url,
        success: function(response) {
            $('#treeview1').treeview({
                selectedBackColor: "#03a9f3",
                onhoverColor: "rgba(0, 0, 0, 0.05)",
                expandIcon: 'ti-angle-right',
                collapseIcon: 'ti-angle-down',
                showBorder: false,
                data: 'test',
                onNodeSelected: function(event, node) {
                    $('#treeview1').treeview('expandNode', [node.nodeId]);

                    $('#node_flag').val(node.dataFlag);

                    let setHeaders = "";
                    $.each(node.dataHeaders, function (index, value) {
                        setHeaders += "<th class=\"text-center\" style=\"vertical-align: middle;\">"+ (value == "id" ? '':value) +"</th>";
                    });

                    $('#main-table').empty();
                    $('#main-table').append('<table id="details-table" class="table table-sm table-bordered nowrap" style="width:100%">' +
                                            '<thead>' +
                                                '<tr id="details-table-headers">' + setHeaders + '</tr>' +
                                            '</thead>' +
                                            '</table>');

                    $('#details-table').DataTable().clear().destroy();

                    loadTable(node.href, node.dataId, node.dataColumns);

                    if (node.dataFlag == "itemtype") {
                        if (node.dataChildren.id === "item") {
                            showFrameItem(node.dataChildren.subTableUrl);
                            
                            sessionStorage.setItem(node.dataChildren.storage.selectedItem.id, node.dataChildren.storage.selectedItem.value);
                            sessionStorage.setItem(node.dataChildren.storage.detailsItem.id, node.dataChildren.storage.detailsItem.value);
                        }
                    }
                },
                onNodeUnselected: function(event, node) {
                    $('#node_flag').val('');

                    let setHeaders = "";
                    $.each(node.dataHeaders, function (index, value) {
                        setHeaders += "<th class=\"text-center\" style=\"vertical-align: middle;\">"+ (value == "id" ? '':value) +"</th>";
                    });

                    $('#main-table').empty();
                    $('#main-table').append('<table id="details-table" class="table table-sm table-bordered nowrap" style="width:100%">' +
                                            '<thead>' +
                                                '<tr id="details-table-headers">' + setHeaders + '</tr>' +
                                            '</thead>' +
                                            '</table>');

                    $('#details-table').DataTable().clear().destroy();

                    loadTable(node.hrefUnselected, node.dataId, node.dataColumns);
                },
                data: response
            });
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function expandNodes(nodeId) {
    $('#treeview1').treeview('expandNode', [nodeId]);
}