<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('theme/assets/images/PAM_logo.png') }}">
    <title>{{ $pageAttributes['title'] }}</title>

    <!-- page css -->
    <link href="{{ asset('theme/dist/css/pages/login-register-lock.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('theme/dist/css/style.min.css') }}" rel="stylesheet">
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">VIMS</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background-image:url({{ asset('theme/assets/images/login-register.jpg') }});">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material text-center" id="loginform" action="{{ route('authprocess') }}"
                    method="POST">
                    @csrf
                    <a href="javascript:void(0)" class="db">
                        <img src="{{ asset('theme/assets/images/PAM_img.png') }}" height="30px" alt="Home" />
                        <br /><br />
                        <h3 class="text-info font-weight-bold">PAM</h3>
                        <span class="text-info">Plant Asset Management</span>
                    </a>
                    @if ($errors->any())
                        <div class="alert alert-danger px-0 pt-3 pb-0 m-t-40">
                            <ul class="text-left">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" name="username" type="text" required placeholder="Username"
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <input class="form-control" name="password" id="password" type="password" required
                                    placeholder="Password" autocomplete="off" style="z-index:100">
                                <i class="fas fa-eye-slash" id="show"
                                    style="cursor: pointer; margin-left: -30px; z-index: 100; margin-top:13px; color:#6D8299"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="rememberCheck"
                                        id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                </div>
                                <div class="ml-auto">
                                    <a href="javascript:void(0)" id="to-recover" class="text-muted"><i
                                            class="fas fa-lock m-r-5"></i> Forgot Password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-5">
                        {{-- {!! NoCaptcha::renderJs('en') !!}
                        {!! NoCaptcha::display() !!} --}}
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Log
                                In</button>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <a href="{{ route('wellcome') }}" class="text-reset">Back to wellcome page</a>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="recovery-msg-content">

                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="recoveryemail form-control" type="text" required="" name="email_recovery"
                                placeholder="Email" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button
                                class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light account_recovery"
                                type="button">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- jQuery -->
    <script src="{{ asset('theme/assets/node_modules/jquery/jquery-3.6.0.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('theme/assets/node_modules/popper/popper.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });

        $(document).ready(function() {
            $('#show').on('click', function() {
                const type = $('#password').attr('type') === 'password' ? 'text' : 'password';
                $('#password').attr('type', type);

                if ($('#show').hasClass('fa-eye-slash')) {
                    $('#show').removeClass('fa-eye-slash');
                    $('#show').addClass('fa-eye');
                } else {
                    $('#show').removeClass('fa-eye');
                    $('#show').addClass('fa-eye-slash');
                }
            });

            $(document).on('click', '.account_recovery', function(e) {
                e.preventDefault();

                var data = {
                    'email': $('.recoveryemail').val()
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "recoveryprocess",
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        if (response.status == 400) {
                            $('.recovery-msg-content').addClass('alert alert-danger');
                            $('.recovery-msg-content').text(response.errors.email);
                        } else if (response.status == 200) {
                            $('.recovery-msg-content').addClass('alert alert-success');
                            $('.recovery-msg-content').text(response.errors);

                            setTimeout(() => {
                                $("#loginform").fadeIn();
                                $("#recoverform").slideUp();
                            }, 1500);
                        }
                    }
                });
            });
        });
    </script>

</body>

</html>
