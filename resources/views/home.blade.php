@extends('layouts.main')
@section('title', 'Home')
@section('css')
    <link href="{{ asset('theme/dist/css/pages/chat-app-page.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/node_modules/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/node_modules/select2/dist/css/select2.css') }}" rel="stylesheet" />
@endsection
@section('content')
    <div class="row pb-0">
        <!-- Left Frame -->
        <div class="col-2 m-0 p-0">
            <div class="card mb-0">
                <div class="card-body bg-light">
                    Asset Explorer
                </div>
                <div class="card-body bg-white" style="height: 720px;">
                    <div class="row">
                        <div class="col-12" id="scrollAssetFrame" style="height: 42.6rem;">
                            <div id="treeview1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Middle Frame -->
        <div class="col-6 m-0 px-1">
            <div class="card mb-0">
                <div class="card-body bg-light">
                    Browser
                </div>
                <div class="card-body bg-white p-t-1" style="height: 720px;">
                    <form id="grid_parameters">
                        <input type="text" id="node_flag" name="node_flag" placeholder="node-flag" readonly>
                        <input type="checkbox" id="filterCheckbox">&nbsp;<span>Filter</span>
                    </form>
                    <div class="table-responsive m-t-0" id="main-table"></div>
                </div>
            </div>
        </div>
        <!-- Right Frame -->
        <div class="col-4 m-0 p-0">
            <div class="card mb-0">
                <div class="card-body bg-light">
                    Detail Information
                </div>
                <div class="card-body bg-white pr-4" style="height: 720px;">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body px-0 py-0">
                                    <!-- Form Item -->
                                    <form id="details_info_item" action="#" style="display: none;">
                                        <div class="row">
                                            <div class="col-12 text-right pb-3">
                                                <a href="#" class="btn btn-md btn-secondary">Add</a>
                                                <a href="#" class="btn btn-md btn-secondary" onclick="editFormItem()">Edit</a>
                                                <a href="#" class="btn btn-md btn-secondary">Delete</a>
                                            </div>
                                            <div class="col-12">
                                                <select 
                                                    class="form-control select2-tags-item"
                                                    id="tags_item" 
                                                    name="tags_Item" 
                                                    data-show="{{ route('showitemsonselectbox') }}"
                                                    data-refference="item" style="width: 100%;">
                                                        <option selected disabled value='0'>Select #Tag</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 pt-2 pb-2">
                                                <select 
                                                    class="form-control select2-section-name"
                                                    id="item_sections" 
                                                    name="item_sections" style="width: 100%;">
                                                        <option>General Information</option>
                                                        <option>Other Information</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row border-top pt-2">
                                            <div class="col-12" id="item_content" style="height:30rem">
                                                <form id="section_item_general" action="#" style="display: visible;">
                                                    <input type="text" id="item_id" name="item_id">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Field 1</label>
                                                                <input type="text" id="item_field_1" name="item_field_1" class="form-control" value="{{ old('field_1') }}" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Field 2</label>
                                                                <input type="text" id="item_field_2" name="item_field_2" class="form-control" value="{{ old('field_2') }}" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Field 3</label>
                                                                <input type="text" id="item_field_3" name="item_field_3" class="form-control" value="{{ old('field_3') }}" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="table-responsive m-t-0">
                                                                <table id="item-sub1-table" class="table table-sm table-bordered nowrap" style="width:100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="text-center" style="vertical-align: middle"></th>
                                                                            <th class="text-center" style="vertical-align: middle">Field 1</th>
                                                                            <th class="text-center" style="vertical-align: middle">Field 2</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('theme/assets/node_modules/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/datatables/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/select2/dist/js/select2.full.js') }}" type="text/javascript">
    <script src="{{ asset('theme/dist/js/pages/chat.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/bootstrap-treeview-master/dist/bootstrap-treeview.min.js') }}"></script>
    <script src="{{ asset('theme/components/asset-explorer/js/tree-config.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        let treeUrl = "{{ route('treemenus') }}";
        loadTreeContent(treeUrl);

        $(function () {
            let urlInitial = "{{ route('companylist') }}";
            
            loadInitialData(urlInitial);
        });
    </script>
    <script src="{{ asset('theme/components/browser/js/tablegrid-config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme/components/detail-information/js/item/form-item-all.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme/components/detail-information/js/item/tablegrid-item-config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme/components/detail-information/js/item/selectbox-tags.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme/components/detail-information/js/item/selectbox-sectionname.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme/assets/node_modules/bootstrap-treeview-master/dist/bootstrap-treeview-init.js') }}"></script>
@endsection
