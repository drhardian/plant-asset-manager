<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('theme/assets/images/PAM_logo.png') }}">
    <title>{{ $pageAttributes['title'] }}</title>

    <link rel="stylesheet" href="/theme/bootstrap-3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/theme/assets/font-awesome-6/css/all.css">
    <link rel="stylesheet" href="/theme/assets/boxass/css/flaticon-set.css">
    <link rel="stylesheet" href="/theme/assets/boxass/css/magnific-popup.css">
    <link rel="stylesheet" href="/theme/assets/boxass/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/theme/assets/boxass/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/theme/assets/boxass/css/animate.css">
    <link rel="stylesheet" href="/theme/assets/boxass/css/bootsnav.css">
    <link rel="stylesheet" href="/theme/assets/boxass/css/style.css">
    <link rel="stylesheet" href="/theme/assets/boxass/css/responsive.css">

    <!-- ========== Google Fonts ========== -->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800&display=swap" rel="stylesheet">

    <style>
        .dropdown-submenu {
            position: relative;
        }
    </style>

</head>
<body>

    <!-- Preloader Start -->
    <div class="se-pre-con"></div>
    <!-- Preloader Ends -->
    
    <!-- Header 
    ============================================= -->
    <header id="home">

        <!-- Start Navigation -->
        <nav class="navbar navbar-default navbar-fixed dark no-background bootsnav">

            <div class="container">

                <!-- Start Atribute Navigation -->
                <div class="attr-nav button-light">
                    <ul>
                        <li>
                            <a href="{{ route('login') }}">login</a>
                        </li>
                        <li>
                            <a class="smooth-menu" href="#pricing">Sign Up</a>
                        </li>
                    </ul>
                </div>
                <!-- End Atribute Navigation -->

                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="index.html">
                        <img src="/theme/assets/images/PAM6.png" class="logo logo-display" alt="Logo">
                        <img src="/theme/assets/images/PAM6.png" class="logo logo-scrolled" alt="Logo">
                    </a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav navbar-right" data-in="#" data-out="#">
                        <li>
                            <a class="smooth-menu" href="#home">Home</a>
                        </li>
                        <li>
                            <a class="smooth-menu" href="#about">About</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-expanded="false" >Services <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Valve Management Solution</a></li>
                                <li><a href="#">Asset Management Software</a></li>
                                <li><a href="#">Ask The Expert</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-expanded="false" >News & Events <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="smooth-menu" href="#news">News</a></li>
                                <li><a href="#">Webinar</a></li>
                                <li><a href="#">Training</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-expanded="false" >Resources <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Video</a></li>
                                <li><a class="smooth-menu" href="#white-paper">White Papers</a></li>
                                <li><a href="#">Case Studies</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-expanded="false" >Our Apps <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a href="#" data-toggle="dropdown" aria-expanded="false">Install Base Manager <span class="fa fa-caret-right"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">IB Dashboard</a></li>
                                        <li><a href="#">IB Data</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="#" data-toggle="dropdown" aria-expanded="false">Site Walkdown <span class="fa fa-caret-right"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Valve</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="#" data-toggle="dropdown" aria-expanded="false">Maintenance <span class="fa fa-caret-right"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Valve Repair</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="smooth-menu" href="#contactus">Contact Us</a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>

        </nav>
        <!-- End Navigation -->

    </header>
    <!-- End Header -->

    <!-- Start Banner 
    ============================================= -->
    <div id="home" class="banner-area fixed-top bg-theme-small bg-cover" style="background-image: url('/theme/assets/boxass/img/shape-bg.jpg');">
        <div class="side-bg">
            <img src="/theme/assets/images/Home_trans_1.png" alt="Thumb">
        </div>
        <div class="box-table">
            <div class="box-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 left-info">
                            <div class="content" data-animation="animated fadeInUpBig">
                                <h1>Time to <span>Manage</span> your assets</h1>
                                <p>
                                    Today, valve and instrument asset management is a difficult process. It is time-consuming and frustrating for technicians to document valves, instruments, other components. 
                                    Asset managers also need to track the repairs, maintenance, and replacements of these assets.<br/>
                                    On top of that, the facility owner wants to know the conditions of the asset, are they in good conditions and operate safely?
                                </p>
                                <a class="btn btn-theme border btn-md" href="#">Try Demo</a>
                                <a class="btn-animation popup-youtube" href="/theme/assets/video/PAM_Intro.mp4">
                                    <i class="fa fa-play"></i> Watch Video
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->

    <!-- Start About Area 
    ============================================= -->
    <div id="about" class="about-area default-padding shadow-less">
        <div class="container">
            <div class="row">
                <div class="about-items">
                    <div class="col-md-6 thumb">
                        <img src="/theme/assets/images/BuildingB-1.png" alt="Thumb">
                    </div>
                    <div class="col-md-6 info">
                        <div class="content">
                            <h1>About Us</h1>
                            <h3>PT. Control System Arena Para Nusa</h3>
                            <p>
                                Established in Jakarta, 29 Feb 1980, as a Domestic Company. 
                            </p>
                            <div class="address-info">
                                <ul>
                                    <li>Office</li>
                                    <p>
                                        Jakarta (HO), Pekanbaru, Cilegon, Surabaya, Balikpapan.
                                    </p>
                                </ul>
                            </div>
                            <div class="address-info">
                                <ul>
                                    <li>Facility</li>
                                    <p>
                                        Engineering Center, Trainning Center, Staging Area, Workshop.
                                    </p>
                                </ul>
                            </div>
                            <div class="address-info">
                                <ul>
                                    <li>Capability</li>
                                    <p>
                                        Planning, Engineering, Proposal, Project Management, Trainning, Product & System Integration, Testing, and Lifecycle Services.
                                    </p>
                                </ul>
                            </div>
                            <div class="address-info">
                                <ul>
                                    <li>Principal</li>
                                    <p>
                                        Emerson Automation Solutions since 1980 (Fisher).
                                    </p>
                                </ul>
                            </div>
                            <div class="address-info">
                                <ul>
                                    <li>Certification</li>
                                    <p>
                                        <i class="fa fa-check-circle"></i> Emerson DCS Lifecycle Services Certified.<br/>
                                        <i class="fa fa-check-circle"></i> Emerson Authorized Service Provider for Control Valve, PSV, Valve, Regulator.<br/>
                                        <i class="fa fa-check-circle"></i> MIGAS Approved Metering System Packager.<br/>
                                    </p>
                                </ul>
                            </div>
                            <div class="achivement-items">
                                <ul>
                                    <li>
                                        <span class="medium">ISO 9001:2015</span>
                                    </li>
                                    <li>
                                        <span class="medium">ISO 14001:2015</span>
                                    </li>
                                    <li>
                                        <span class="medium">ISO 45001:2018</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End About Area -->

    <!-- Start Features Area 
    ============================================= -->
    <!-- <div id="features" class="features-area icon-link carousel-shadow default-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2">
                    <div class="site-heading text-center">
                        <h2>PT. Control System Arena Para Nusa</h2>
                        <p>
                            Established in Jakarta, 29 Feb 1980, as a Domestic Company. 
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="features-items features-carousel owl-carousel owl-theme">
                        <div class="item">
                            <div class="icon">
                                <span>01</span>
                                <i class="flaticon-drag-2"></i>
                            </div>
                            <div class="info">
                                <h4>Office</h4>
                                <p>
                                    Jakarta (HO), Pekanbaru, Cilegon, Surabaya, Balikpapan.
                                </p>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <span>02</span>
                                <i class="flaticon-software"></i>
                            </div>
                            <div class="info">
                                <h4>Facility</h4>
                                <p>
                                    Engineering Center, Trainning Center, Staging Area, Workshop.
                                </p>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <span>03</span>
                                <i class="flaticon-rgb"></i>
                            </div>
                            <div class="info">
                                <h4>Capability</h4>
                                <p>
                                    Planning, Engineering, Proposal, Project Management, Trainning, Product & System Integration, Testing, and Lifecycle Services.
                                </p>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <span>04</span>
                                <i class="flaticon-resolution"></i>
                            </div>
                            <div class="info">
                                <h4>Principal</h4>
                                <p>
                                    Emerson Automation Solutions since 1980 (Fisher).
                                </p>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <span>05</span>
                                <i class="flaticon-resolution"></i>
                            </div>
                            <div class="info">
                                <h4>Certification</h4>
                                <p>
                                    <ul>
                                        <li>Emerson DCS Lifecycle Services Certified</li>
                                        <li>Emerson Authorized Service Provider for Control Valve, PSV, Valve, Regulator</li>
                                    </ul>
                                    MIGAS Approved Metering System Packager.<br/>
                                    ISO 9001:2015, ISO 14001:2015, ISO 45001:2018.
                                </p>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- End Features Area -->
        
    <!-- Start News Area
    ============================================= -->
    <div id="news" class="blog-area default-padding bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2">
                    <div class="site-heading text-center">
                        <h2>Recent News</h2>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam molestiae aut placeat, omnis laboriosam possimus, itaque, voluptates harum exercitationem libero ab incidunt? Et assumenda corporis blanditiis error consequuntur eius sequi.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="blog-items">
                    <div class="news-carousel owl-carousel owl-theme">
                        <div class="single-item">
                            <div class="item">
                                <div class="thumb">
                                    <a href="https://www.emerson.com/en-in/news/automation/21-10-fisher-digital-isolation-final-element-solution"><img src="/theme/assets/images/news_11.jpg" alt="Thumb"></a>
                                </div>
                                <div class="info">
                                    <div class="content">
                                        <div class="date">
                                            19 Oct, 2021
                                        </div>
                                        <h4>
                                            <a href="https://www.emerson.com/en-in/news/automation/21-10-fisher-digital-isolation-final-element-solution">Emerson Introduces Industry’s First Complete SIL 3-Certified Valve Assemblies</a>
                                        </h4>
                                        <p>
                                            Fisher Digital Isolation solutions are SIL-certified as full assemblies by exida and can improve critical performance specifications in safety instrumented systems by up to 50%. 
                                        </p>
                                        <a href="https://www.emerson.com/en-in/news/automation/21-10-fisher-digital-isolation-final-element-solution">Read More <i class="fas fa-angle-right"></i></a>
                                    </div>
                                    <div class="meta">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span>By Admin</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-item">
                            <div class="item">
                                <div class="thumb">
                                    <a href="https://www.emerson.com/en-in/news/automation/20-09-ball-valve"><img src="/theme/assets/images/news_2.png" alt="Thumb"></a>
                                </div>
                                <div class="info">
                                    <div class="content">
                                        <div class="date">
                                            23 Sept, 2020
                                        </div>
                                        <h4>
                                            <a href="https://www.emerson.com/en-in/news/automation/20-09-ball-valve">Emerson’s New Full-Bore Ball Control Valve Combats Vibration, Cavitation and Noise</a>
                                        </h4>
                                        <p>
                                            Fisher™ V280’s simplified construction allows the addition of noise-attenuating or anti-cavitation trims on the valve inlet or outlet.
                                        </p>
                                        <a href="https://www.emerson.com/en-in/news/automation/20-09-ball-valve">Read More <i class="fas fa-angle-right"></i></a>
                                    </div>
                                    <div class="meta">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span>By Admin</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-item">
                            <div class="item">
                                <div class="thumb">
                                    <a href="https://www.emerson.com/en-in/news/automation/1810-fisher-digital-level-controllers"><img src="/theme/assets/images/news_3.jpg" alt="Thumb"></a>
                                </div>
                                <div class="info">
                                    <div class="content">
                                        <div class="date">
                                            24 Oct, 2018
                                        </div>
                                        <h4>
                                            <a href="https://www.emerson.com/en-in/news/automation/1810-fisher-digital-level-controllers">New Digital Level Controllers Enable Hassle-free Calibration and Enhanced Safety Levels</a>
                                        </h4>
                                        <p>
                                            The Fisher™ DLC 3100 digital level controller features intuitive local operator interface for increased ease-of-use. 
                                        </p>
                                        <a href="https://www.emerson.com/en-in/news/automation/1810-fisher-digital-level-controllers">Read More <i class="fas fa-angle-right"></i></a>
                                    </div>
                                    <div class="meta">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span>By Admin</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-item">
                            <div class="item">
                                <div class="thumb">
                                    <a href="https://www.emerson.com/en-sg/automation/events/emerson-exchange-asia-pacific"><img src="/theme/assets/images/news_4.png" alt="Thumb"></a>
                                </div>
                                <div class="info">
                                    <div class="content">
                                        <div class="date">
                                            26 Oct, 2021
                                        </div>
                                        <h4>
                                            <a href="https://www.emerson.com/en-sg/automation/events/emerson-exchange-asia-pacific">DISCOVER. TRANSFORM. DIGITALIZE.</a>
                                        </h4>
                                        <p>
                                            Emerson Exchange Asia Pacific<br/>
                                            VIRTUAL EDITION 2021<br/>
                                            DISCOVER. TRANSFORM. DIGITALIZE.<br/><br/>
                                            October 26-28, 2021 
                                        </p>
                                        <a href="https://www.emerson.com/en-sg/automation/events/emerson-exchange-asia-pacific">Read More <i class="fas fa-angle-right"></i></a>
                                    </div>
                                    <div class="meta">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span>By Admin</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =============================================
    End Blog Area -->

    <!-- Start Pricing Area
    ============================================= -->
    <div id="pricing" class="pricing-area default-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2">
                    <div class="site-heading text-center">
                        <h2>Our Packages</h2>
                        <p>
                            Learning day desirous informed expenses material returned six the. She enabled invited exposed him another. Reasonably conviction solicitude me mr at discretion reasonable. Age out full gate bed day lose. 
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="pricing-items">
                        <div class="col-lg-4 col-md-4 single-item">
                            <div class="pricing-item">
                                <ul>
                                    <li class="pricing-header">
                                        <h4>Standard</h4>
                                        <h2><sup>$</sup>xxxx</h2>
                                        <span>x user</span>
                                    </li>
                                    <li><i class="fas fa-times"></i> On Premise</li>
                                    <li><i class="fas fa-times"></i> Install Base Manager</li>
                                    <li><i class="fas fa-times"></i> Site Walkdown Apps</li>
                                    <li><i class="fas fa-times"></i> Repair Apps</li>
                                    <li><i class="fas fa-check"></i> Documentation</li>
                                    <li class="footer">
                                        <a class="btn circle btn-theme border btn-sm" href="#">Register</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 single-item">
                            <div class="pricing-item active">
                                <ul>
                                    <li class="pricing-header">
                                        <h4><b>Advanced</b></h4>
                                        <h2><sup>$</sup>xxxx</h2>
                                        <span>x user</span>
                                    </li>
                                    <li><i class="fas fa-times"></i> On Premise</li>
                                    <li><i class="fas fa-check"></i> Install Base Manager</li>
                                    <li><i class="fas fa-times"></i> Site Walkdown Apps</li>
                                    <li><i class="fas fa-times"></i> Repair Apps</li>
                                    <li><i class="fas fa-check"></i> Documentation</li>
                                    <li class="footer">
                                        <a class="btn circle btn-theme effect btn-sm" href="#">Register</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 single-item">
                            <div class="pricing-item">
                                <ul>
                                    <li class="pricing-header">
                                        <h4>Premium</h4>
                                        <h2><sup>$</sup>xxxx</h2>
                                        <span>Unlimited user</span>
                                    </li>
                                    <li><i class="fas fa-check"></i> On Premise</li>
                                    <li><i class="fas fa-check"></i> Install Base Manager</li>
                                    <li><i class="fas fa-check"></i> Site Walkdown Apps</li>
                                    <li><i class="fas fa-check"></i> Repair Apps</li>
                                    <li><i class="fas fa-check"></i> Documentation</li>
                                    <li class="footer">
                                        <a class="btn circle btn-theme border btn-sm" href="#">Register</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Pricing Area -->

    <!-- Start White Papers
    ============================================= -->
    <div id="white-paper" class="testimonials-area bg-gray default-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2">
                    <div class="site-heading text-center">
                        <h2>White Papers</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="testimonial-items testimonial-carousel owl-carousel owl-theme">
                        <!-- Single Item -->
                        <div class="item">
                            <div class="thumb">
                                <img src="/theme/assets/images/WP-1.PNG" alt="Thumb">
                            </div>
                            <div class="info">
                                <p>
                                    Manual Valves cause plant owners difficulties through low visibility, high operational costs, and lack of flexibility for process control. These challenges may often times not be observable, and the resulting costs go unquantified. 
                                </p>
                                <h4>Wireless - Valve Operating System</h4>
                                <a href="/theme/assets/wp/WP-1.pdf" target="_blank">Show Details</a>
                            </div>
                        </div>
                        <!-- End Single Item -->
                        <!-- Single Item -->
                        <div class="item">
                            <div class="thumb">
                                <img src="/theme/assets/images/WP-2.PNG" alt="Thumb">
                            </div>
                            <div class="info">
                                <p>
                                    We implemented Emerson’s Smart Process distillation application and the embedded MPC with the help of their control experts. In the first phase, MPC helps us maintain a project accomplishment of about 18% energy savings overpast performance.
                                </p>
                                <h4>Digital Architecture to Improve Recovery Section Operations</h4>
                                <a href="/theme/assets/wp/WP-2.pdf" target="_blank">Show Details</a>
                            </div>
                        </div>
                        <!-- End Single Item -->
                        <!-- Single Item -->
                        <div class="item">
                            <div class="thumb">
                                <img src="/theme/assets/images/WP-3.PNG" alt="Thumb">
                            </div>
                            <div class="info">
                                <p>
                                    The cracked gas compressor is the single most critical piece ofequipment in an ethylene plant. This asset can cost as much as $50 million and operates 24/7 under demanding conditions. 
                                </p>
                                <h4>Optimize and Protect Cracked Gas Compressors with Smart Automation Technology</h4>
                                <a href="/theme/assets/wp/WP-3.pdf" target="_blank">Show Details</a>
                            </div>
                        </div>
                        <!-- End Single Item -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Testimonials Area -->

    <!-- Start Contact Us & Footer 
    ============================================= -->
    <footer id="contactus" class="default-padding bg-light">
        <div class="container">
            <div class="row">
                <div class="f-items">
                    <div class="col-md-4 col-sm-6 equal-height item">
                        <div class="f-item">
                            <img src="/theme/assets/images/PAM4.png" alt="Logo">
                            <p>
                                Excellence decisively nay man yet impression for contrasted remarkably. There spoke happy for you are out. Fertile how old address.
                            </p>
                            <p>
                                <i>Please write your email and get our amazing updates, news and support</i>
                            </p>
                            <div class="newsletter">
                                <form action="#">
                                    <div class="input-group stylish-input-group">
                                        <input type="email" name="email" class="form-control" placeholder="Enter your e-mail here">
                                        <button type="submit">
                                            <i class="fa fa-paper-plane"></i>
                                        </button>  
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 equal-height item">
                        <div class="f-item link">
                            <h4>Quick LInk</h4>
                            <ul>
                                <li>
                                    <a href="#">Home</a>
                                </li>
                                <li>
                                    <a href="#about">About us</a>
                                </li>
                                <li>
                                    <a href="#">Company History</a>
                                </li>
                                <li>
                                    <a href="#">Management </a>
                                </li>
                                <li>
                                    <a href="#">Features</a>
                                </li>
                                <li>
                                    <a href="#">Blog Page</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 equal-height item">
                        <div class="f-item twitter-widget">
                            <h4>Contact Info</h4>
                            <img src="/theme/assets/images/ptcs.png" alt="Logo" height="80px"><br/>
                            <span class="company-name">PT. CONTROL SYSTEM ARENA PARA NUSA</span>
                            <p>
                                Beltway Office Park, Tower B 3rd Floor, Jl. Ampera Raya No.9-10<br/>
                                Jakarta Selatan 12550 Indonesia
                            </p>
                            <div class="address">
                                <ul>
                                    <li>
                                        <div class="icon">
                                            <i class="fas fa-home"></i> 
                                        </div>
                                        <div class="info">
                                            <h5>Website:</h5>
                                            <span>www.ptcs.co.id</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <i class="fas fa-envelope"></i> 
                                        </div>
                                        <div class="info">
                                            <h5>Email:</h5>
                                            <span>sales@ptcs.co.id</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <i class="fas fa-phone"></i> 
                                        </div>
                                        <div class="info">
                                            <h5>Phone:</h5>
                                            <span>+62-21-780-7881</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Footer Bottom -->
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="col-lg-6 col-md-6 col-sm-7">
                            <p>&copy; Copyright 2021. All Rights Reserved by <a href="#">PTCS</a></p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-5 text-right link">
                            <ul>
                                <!--<li>
                                    <a href="#">Terms of user</a>
                                </li>
                                <li>
                                    <a href="#">License</a>
                                </li>
                                <li>
                                    <a href="#">Support</a>
                                </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Footer Bottom -->
        </div>
    </footer>
    <!-- End Footer -->

    <!-- production purposes -->
    <script src="/theme/assets/boxass/js/jquery-1.12.4.min.js"></script>
    <script src="/theme/bootstrap-3.4.1/js/bootstrap.min.js"></script>
    <script src="/theme/assets/boxass/js/equal-height.min.js"></script>
    <script src="/theme/assets/boxass/js/jquery.appear.js"></script>
    <script src="/theme/assets/boxass/js/jquery.easing.min.js"></script>
    <script src="/theme/assets/boxass/js/jquery.magnific-popup.min.js"></script>
    <script src="/theme/assets/boxass/js/modernizr.custom.13711.js"></script>
    <script src="/theme/assets/boxass/js/owl.carousel.min.js"></script>
    <script src="/theme/assets/boxass/js/count-to.js"></script>
    <script src="/theme/assets/boxass/js/wow.min.js"></script>
    <script src="/theme/assets/boxass/js/bootsnav.js"></script>
    <script src="/theme/assets/boxass/js/main.js"></script>

</body>
</html>