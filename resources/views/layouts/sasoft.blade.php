<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>PAM - Sasoft Theme</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="{{asset('theme/dist/css/font-awesome/css/all.css')}}" rel="stylesheet" />
    <link href="{{asset('theme/dist/css/animate.css')}}" rel="stylesheet" />
    <link href="{{asset('theme/dist/css/bootsnav.css')}}" rel="stylesheet" />
    <link href="{{asset('theme/dist/css/style_sasoft.css')}}" rel="stylesheet" />
    <link href="{{asset('theme/dist/css/responsive_sasoft.css')}}" rel="stylesheet" />
</head>
<body>

    <header id="home">

        <!-- Start Navigation -->
        <nav class="navbar navbar-default attr-bg navbar-fixed dark no-background bootsnav">

            <div class="container">

                <!-- Start Atribute Navigation -->
                <div class="attr-nav light">
                    <ul>
                        <li class="button">
                            <a href="#">Sign Up</a>
                        </li>
                    </ul>
                </div>        
                <!-- End Atribute Navigation -->

                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="index.html">
                        <img src="{{asset('theme/assets/images/PAM_img.png')}}" class="logo" alt="Logo" width="200">
                    </a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
                        <li>
                            <a href="#home" class="smooth-menu active" >Home</a>
                        </li>
                        <li>
                            <a class="smooth-menu" href="#features">Features</a>
                        </li>
                        <li>
                            <a class="smooth-menu" href="#overview">Overview</a>
                        </li>
                        <li>
                            <a class="smooth-menu" href="#team">Team</a>
                        </li>
                        <li>
                            <a class="smooth-menu" href="#pricing">Pricing</a>
                        </li>
                        <li class="dropdown dropdown-right">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Pages</a>
                            <ul class="dropdown-menu">
                                <li><a target="_blank" href="login.html">Login</a></li>
                                <li><a target="_blank" href="register.html">Register</a></li>
                                <li><a href="404.html">Error 404</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="smooth-menu" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <!-- End Navigation -->

    </header>

    <!-- Start Banner 
    ============================================= -->
    <div class="banner-area responsive-top-pad inc-shape text-default">
        <div class="container">
            <div class="double-items">
                <div class="row align-center">

                    <div class="col-lg-6 info shape">
                        <h2 class="wow fadeInDown" data-wow-duration="1s">We're building software <strong>to manage business</strong></h2>
                        <p class="wow fadeInLeft" data-wow-duration="1.5s">
                            Contented continued any happiness instantly objection yet her allowance. Use correct day new brought tedious.
                        </p>
                        <div class="bottom">
                            <a class="btn btn-md btn-gradient wow fadeInDown" data-wow-duration="1.8s" href="#">Get Started</a>
                            <a href="https://www.youtube.com/watch?v=owhuBrGIOsE" class="popup-youtube video-btn wow fadeInUp"><i class="fa fa-play"></i>Watch Video</a>
                        </div>
                    </div>

                     <div class="col-lg-5 offset-lg-1 width-140 thumb wow fadeInRight" data-wow-duration="1s">
                        <img src="assets/img/illustration/2.png" alt="Thumb">
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->

    <script src="{{asset('theme/dist/js/jquery-1.12.4.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="{{asset('theme/dist/js/bootsnav.js')}}"></script>
    <script src="{{asset('theme/dist/js/main.js')}}"></script>
</body>
</html>