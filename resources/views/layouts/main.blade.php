<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('theme/assets/images/Pertamina-1.png') }}">
    <title>PAM - @yield('title')</title>
    <!--Toaster Popup message CSS -->
    <link href="{{ asset('theme/assets/node_modules/bootstrap-5.0.2/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/node_modules/toast-master/css/toastr.min.css') }}" rel="stylesheet">
    <!-- Popup CSS -->
    <link href="{{ asset('theme/assets/node_modules/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
    <!-- Additional CSS -->
    @yield('css')
    <!-- Custom CSS -->
    <link href="{{ asset('theme/dist/css/style-main.min.css') }}" rel="stylesheet">
    <style>
        .dataTables_wrapper .dataTables_processing {
            position: absolute;
            top: 30%;
            left: 50%;
            width: 30%;
            height: 40px;
            margin-left: -20%;
            margin-top: -25px;
            padding-top: 20px;
            text-align: center;
            font-size: 1.2em;
            background: none;
            color: #FE7E6D;
        }
    </style>
</head>

<body class="horizontal-nav skin-megna fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <img src="{{ asset('theme/assets/images/PAM_logo.png') }}" alt="PHE OSES" width="80rem" height="30rem"
                style="position:relative;left:-80%;margin-top:0.5rem;">
        </div>
    </div>

    <div id="main-wrapper">
        @include('layouts.header')
        @include('layouts.sidebar')
        <div class="page-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        @include('layouts.footer')
    </div>

    <!-- jQuery -->
    {{-- <script src="{{ asset('theme/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script> --}}
    <script src="{{ asset('theme/assets/node_modules/jquery/jquery-3.6.0.min.js') }}"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="{{ asset('theme/assets/node_modules/popper/popper.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/bootstrap-5.0.2/js/bootstrap.js') }}"></script>
    <!-- bootstrap confirmation2 -->
    <script src="{{ asset('theme/dist/js/bootstrap-confirmation.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('theme/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('theme/dist/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('theme/dist/js/sidebarmenu.js') }}"></script>
    <!--sparkline -->
    <script src="{{ asset('theme/assets/node_modules/sparkline/jquery.sparkline.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('theme/dist/js/custom.min.js') }}"></script>
    <!-- Popup message jquery -->
    <script src="{{ asset('theme/assets/node_modules/toast-master/js/toastr.min.js') }}"></script>
    <!-- Sweet Alert jquery -->
    <script src="{{ asset('theme/assets/node_modules/sweetalert/sweetalert2.all.min.js') }}"></script>
    <!-- jquery Validation -->
    <script src="{{ asset('theme/assets/node_modules/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- Magnific popup JavaScript -->
    <script src="{{ asset('theme/assets/node_modules/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>    
    <!--input mask -->
    {{-- <script src="{{ asset('theme/dist/js/jquery.inputmask.js') }}"></script> --}}

    <script>
        $(document).ready(function() {
            toastr.options = {
                "showMethod": "slideDown",
                "hideMethod": "slideUp",
                "progressBar": true,
                "positionClass": "toast-top-center",
                "closeButton": true
            }
        });
    </script>
    {{-- @include('sweetalert::alert') --}}
    @yield('javascript')
</body>

</html>
