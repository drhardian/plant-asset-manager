<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Treecontent extends Model
{
    use HasFactory;
    protected $guarded = [];
}
