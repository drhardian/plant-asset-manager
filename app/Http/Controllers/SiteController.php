<?php

namespace App\Http\Controllers;

use App\Models\Site;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class SiteController extends Controller
{
    public function show(Request $request)
    {
        return Site::find($request->id);
    }

    public function showSites(Request $request)
    {
        $query = DB::table('sites')->select('id','description')->where('company_id', $request->company_id);
        $records = $query->get();

        return DataTables::of($records)
            ->addColumn('action', function ($records) {
                return "<i class=\"fas fa-edit text-info\" style=\"cursor:pointer;\" onclick=\"editInfo('".$records->id."','".route('sitedetails')."')\"></i>&nbsp;<i class=\"fas fa-trash-alt text-danger\" style=\"cursor:pointer;\"></i>";
            })
            ->addColumn('description', function ($records) {
                return $records->description;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
