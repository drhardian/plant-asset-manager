<?php

namespace App\Http\Controllers;

use App\Models\Plant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class PlantController extends Controller
{
    public function show(Request $request)
    {
        return Plant::find($request->id);
    }

    public function showPlants(Request $request)
    {
        $query = DB::table('plants')->select('id','description')
            ->where([
                'company_id' => $request->company_id,
                'site_id' => $request->site_id
            ]);
        $records = $query->get();

        return DataTables::of($records)
            ->addColumn('action', function ($records) {
                return "<i class=\"fas fa-edit text-info\" style=\"cursor:pointer;\" onclick=\"editInfo('".$records->id."','".route('plantdetails')."')\"></i>&nbsp;<i class=\"fas fa-trash-alt text-danger\" style=\"cursor:pointer;\"></i>";
            })
            ->addColumn('description', function ($records) {
                return $records->description;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
