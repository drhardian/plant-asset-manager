<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class AssetController extends Controller
{
    public function showAssets(Request $request)
    {
        $query = DB::table('assets')
            ->select(
                'description'
            )
            ->where([
                'company_id' => $request->company_id,
                'site_id' => $request->site_id,
                'plant_id' => $request->plant_id
            ]);

        $records = $query->get();

        return DataTables::of($records)
            ->addColumn('action', function ($records) {
                return "<i class=\"fas fa-edit text-info\" style=\"cursor:pointer;\"></i>&nbsp;<i class=\"fas fa-trash-alt text-danger\" style=\"cursor:pointer;\"></i>";
            })
            ->addColumn('description', function ($records) {
                return $records->description;
            })
            ->make(true);
    }
}
