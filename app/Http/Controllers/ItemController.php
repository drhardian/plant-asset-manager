<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ItemController extends Controller
{
    public function showItemsOnGrid(Request $request)
    {
        $query = DB::table('items')->where([
                'company_id' => $request->company_id,
                'site_id' => $request->site_id,
                'plant_id' => $request->plant_id,
                'asset_id' => $request->asset_id,
                'itemtype_id' => $request->itemtype_id
            ]);

        $records = $query->get();

        return DataTables::of($records)
            ->addColumn('action', function ($records) {
                return "<input type=\"checkbox\">";
            })
            ->addColumn('tableType', function () {
                return "item";
            })
            ->removeColumn(['asset_id','company_id','created_at','itemtype_id','plant_id','site_id','updated_at'])
            ->make(true);
    }

    public function showItemsOnSelectbox(Request $request)
    {
        $search = $request->search;
        $initialReff = $request->tag_refference;
        $decryptReff = Crypt::decryptString($initialReff);
        $tagReff = explode("|", $decryptReff);

        $query = Item::where('company_id', $tagReff[0])
                ->where('site_id', $tagReff[1])
                ->where('plant_id', $tagReff[2])
                ->where('asset_id', $tagReff[3])
                ->where('itemtype_id', $tagReff[4])
                ->orderBy('field_1');
        $query->when($search, function ($query, $search) {
            return $query->where('field_1', 'like', '%'.$search.'%');
        });
        $datas = $query->get();

        $response = [];

        foreach($datas as $data){
            $response[] = array(
                "id" => $data->id,
                "text" => $data->field_1
            );
        }

        return response()->json($response);
    }

    public function showSelectedItemOnSelectbox(Request $request)
    {
        $item = Item::findOrFail($request->id);

        #Create tag refference in string with separated flag "|"
        # - 0: Company ID
        # - 1: Site ID
        # - 2: Plant ID
        # - 3: Asset ID
        # - 4: Item Type ID
        $reffString = $item->company_id."|".$item->site_id."|".$item->plant_id."|".$item->asset_id."|".$item->itemtype_id;
        $reffEncrypt = Crypt::encryptString($reffString);

        return response()->json([
            'tagRefference' => $reffEncrypt,
            'selectedTag' => [
                'id' => $request->id,
                'text' => $item->field_1
            ]
        ]);
    }

    public function show(Request $request)
    {
        $query = Item::findOrFail($request->id);

        return response()->json([
            'item_details' => [
                'id' => $query->id,
                'field_1' => $query->field_1,
                'field_2' => $query->field_2,
                'field_3' => $query->field_3,
            ],
            'subitem_details' => [
                'url' => route('showsubitemsongrid'),
                
            ]
        ]);
    }
}
