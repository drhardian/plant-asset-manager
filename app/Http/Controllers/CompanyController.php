<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CompanyController extends Controller
{
    public function show(Request $request)
    {
        return Company::find($request->id);
    }

    public function showCompanies(Request $request)
    {
        $search = $request['search']['value'];
        $checked = $request['filter'];

        $query = DB::table('companies')->select('id','description');

        if($checked == 'true') {
            $query->when($search, function ($q, $search) {
                return $q->where('description', $search);
            });
        } else {
            $query->when($search, function ($q, $search) {
                return $q->where('description', 'like', '%'.$search.'%');
            });
        }

        $records = $query->get();

        return DataTables::of($records)
            ->addColumn('action', function ($records) {
                return "<i class=\"fas fa-edit text-info\" style=\"cursor:pointer;\" onclick=\"editInfo('".$records->id."','".route('companydetails')."')\"></i>&nbsp;<i class=\"fas fa-trash-alt text-danger\" style=\"cursor:pointer;\"></i>";
            })
            ->addColumn('description', function ($records) {
                return $records->description;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
