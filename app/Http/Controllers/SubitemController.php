<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class SubitemController extends Controller
{
    public function showSubItemsOnGrid(Request $request)
    {
        $query = DB::table('subitems')->where('item_id', $request->id);

        $records = $query->get();

        return DataTables::of($records)
            ->addColumn('action', function ($records) {
                return "<input type=\"checkbox\">";
            })
            ->addColumn('tableType', function () {
                return "subitem";
            })
            ->removeColumn(['created_at','updated_at'])
            ->make(true);
    }
}
