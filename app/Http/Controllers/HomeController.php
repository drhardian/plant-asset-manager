<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function index_wellcome()
    {
        $attributes = [
            'title' => 'PAM - Plant Asset Management'
        ];

        return view('layouts.app', [
            'pageAttributes' => $attributes
        ]);
    }
}
