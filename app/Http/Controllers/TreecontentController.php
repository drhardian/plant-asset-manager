<?php

namespace App\Http\Controllers;

use App\Http\Traits\ItemTrait;
use App\Models\Asset;
use App\Models\Company;
use App\Models\Item;
use App\Models\Itemtype;
use App\Models\Plant;
use App\Models\Site;

class TreecontentController extends Controller
{
    use ItemTrait;

    public function show()
    {
        return $this->loadCompany();
    }

    public function loadCompany()
    {
        $trees = array();
        $companies = Company::orderBy('description')->get();

        foreach ($companies as $company) {
            $countSite = Site::where('company_id', $company->id)->count();

            if($countSite > 0) {
                $setObject = (object) array(
                    'text' => $company->description,
                    'href' => route('sitelist'),
                    'hrefUnselected' => route('companylist'),
                    'tags' => $countSite,
                    'nodes' => $this->loadSite($company->id),
                    'dataFlag' => 'company',
                    'dataId' => [
                        'company_id' => $company->id
                    ]
                );
            } else {
                $setObject = (object) array(
                    'text' => $company->description,
                    'href' => route('sitelist'),
                    'hrefUnselected' => route('companylist'),
                    'tags' => '0',
                    'dataFlag' => 'company',
                    'dataId' => [
                        'company_id' => $company->id
                    ]
                );
            }

            array_push($trees, $setObject);
        }

        return $trees;
    }

    public function loadSite($companyid)
    {
        $trees = [];
        $sites = Site::where('company_id', $companyid)->orderBy('description')->get();

        foreach ($sites as $site) {
            $countPlant = Plant::where(['company_id' => $companyid, 'site_id' => $site->id])->count();

            if($countPlant > 0) {
                $setObject = (object) array(
                    'text' => $site->description,
                    'href' => route('plantlist'),
                    'hrefUnselected' => route('sitelist'),
                    'tags' => '0',
                    'nodes' => $this->loadPlant($companyid, $site->id),
                    'dataFlag' => 'site',
                    'dataId' => [
                        'company_id' => $companyid,
                        'site_id' => $site->id
                    ]
                );
            } else {
                $setObject = (object) array(
                    'text' => $site->description,
                    'href' => route('plantlist'),
                    'hrefUnselected' => route('sitelist'),
                    'tags' => '0',
                    'dataFlag' => 'site',
                    'dataId' => [
                        'company_id' => $companyid,
                        'site_id' => $site->id
                    ]
                );
            }

            array_push($trees, $setObject);
        }

        return $trees;
    }

    public function loadPlant($companyid, $siteid)
    {
        $trees = [];
        $plants = Plant::where(['company_id' => $companyid, 'site_id' => $siteid])->orderBy('description')->get();

        foreach ($plants as $plant) {
            $countAsset = Asset::where([
                'company_id' => $companyid, 
                'site_id' => $siteid, 
                'plant_id' => $plant->id
            ])->count();

            if($countAsset > 0) {
                $setObject = (object) array(
                    'text' => $plant->description,
                    'href' => route('assetlist'),
                    'hrefUnselected' => route('plantlist'),
                    'tags' => '0',
                    'nodes' => $this->loadAsset($companyid, $siteid, $plant->id),
                    'dataFlag' => 'plant',
                    'dataId' => [
                        'company_id' => $companyid,
                        'site_id' => $siteid,
                        'plant_id' => $plant->id
                    ]
                );
            } else {
                $setObject = (object) array(
                    'text' => $plant->description,
                    'href' => route('assetlist'),
                    'hrefUnselected' => route('plantlist'),
                    'tags' => '0',
                    'dataFlag' => 'plant',
                    'dataId' => [
                        'company_id' => $companyid,
                        'site_id' => $siteid,
                        'plant_id' => $plant->id
                    ]
                );
            }

            array_push($trees, $setObject);
        }

        return $trees;
    }

    public function loadAsset($companyid, $siteid, $plantid)
    {
        $trees = [];
        $assets = Asset::where([
            'company_id' => $companyid, 
            'site_id' => $siteid,
            'plant_id' => $plantid
        ])->orderBy('description')->get();

        foreach ($assets as $asset) {
            $countType = Itemtype::where([
                'company_id' => $companyid, 
                'site_id' => $siteid, 
                'plant_id' => $plantid,
                'asset_id' => $asset->id
            ])->count();

            if($countType > 0) {
                $setObject = (object) array(
                    'text' => $asset->description,
                    'href' => route('itemtypelist'),
                    'hrefUnselected' => route('assetlist'),
                    'tags' => '0',
                    'nodes' => $this->loadItemtype($companyid, $siteid, $plantid, $asset->id),
                    'dataFlag' => 'asset',
                    'dataId' => [
                        'company_id' => $companyid,
                        'site_id' => $siteid,
                        'plant_id' => $plantid,
                        'asset_id' => $asset->id
                    ]
                );
            } else {
                $setObject = (object) array(
                    'text' => $asset->description,
                    'href' => route('itemtypelist'),
                    'hrefUnselected' => route('assetlist'),
                    'tags' => '0',
                    'dataFlag' => 'asset',
                    'dataId' => [
                        'company_id' => $companyid,
                        'site_id' => $siteid,
                        'plant_id' => $plantid,
                        'asset_id' => $asset->id
                    ]
                );
            }

            array_push($trees, $setObject);
        }

        return $trees;
    }

    public function loadItemtype($companyid, $siteid, $plantid, $assetid)
    {
        $trees = [];
        $itemtypes = Itemtype::where([
            'company_id' => $companyid, 
            'site_id' => $siteid,
            'plant_id' => $plantid,
            'asset_id' => $assetid
        ])->orderBy('description')->get();

        foreach ($itemtypes as $itemtype) {
            $countItem = Item::where([
                'company_id' => $companyid, 
                'site_id' => $siteid, 
                'plant_id' => $plantid,
                'asset_id' => $assetid,
                'itemtype_id' => $itemtype->id
            ])->count();

            if($countItem > 0) {
                $setObject = (object) array(
                    'text' => $itemtype->description,
                    'href' => route('showitemsongrid'),
                    'hrefUnselected' => route('itemtypelist'),
                    'tags' => '0',
                    'nodes' => $this->loadItem($companyid, $siteid, $plantid, $assetid, $itemtype->id),
                    'dataFlag' => 'itemtype',
                    'dataId' => [
                        'company_id' => $companyid,
                        'site_id' => $siteid,
                        'plant_id' => $plantid,
                        'asset_id' => $assetid,
                        'itemtype_id' => $itemtype->id
                    ],
                    'dataHeaders' => $this->showItemHeaders(),
                    'dataColumns' => $this->showItemColumns(),
                    'dataChildren' => [
                        'id' => 'item',
                        'storage' => [
                            'selectedItem' => [
                                'id' => 'selecteditem_url',
                                'value' => route('showselecteditemonselectbox')
                            ],
                            'detailsItem' => [
                                'id' => 'detailsitem_url',
                                'value' => route('details_item')
                            ]
                        ],
                        'subTableUrl' => route('showsubitemsongrid')
                    ]
                );
            } else {
                $setObject = (object) array(
                    'text' => $itemtype->description,
                    'href' => route('showitemsongrid'),
                    'hrefUnselected' => route('itemtypelist'),
                    'tags' => '0',
                    'dataFlag' => 'itemtype',
                    'dataId' => [
                        'company_id' => $companyid,
                        'site_id' => $siteid,
                        'plant_id' => $plantid,
                        'asset_id' => $assetid,
                        'itemtype_id' => $itemtype->id
                    ],
                    'dataHeaders' => $this->showItemHeaders(),
                    'dataColumns' => $this->showItemColumns(),
                    'childrenStorage' => [
                        'selectedItem' => [
                            'storageName' => 'selecteditem_url',
                            'storageValue' => route('showselecteditemonselectbox')
                        ],
                        'detailsItem' => [
                            'storageName' => 'detailsitem_url',
                            'storageValue' => route('details_item')
                        ]
                    ]
                );
            }

            array_push($trees, $setObject);
        }

        return $trees;
    }

    public function loadItem($companyid, $siteid, $plantid, $assetid, $itemtypeid)
    {
        $trees = [];
        $items = Item::where([
            'company_id' => $companyid, 
            'site_id' => $siteid,
            'plant_id' => $plantid,
            'asset_id' => $assetid,
            'itemtype_id' => $itemtypeid
        ])->orderBy('field_1')->get();

        foreach ($items as $item) {
            $setObject = (object) array(
                'text' => $item->field_1,
                'href' => '#',
                'hrefUnselected' => route('showitemsongrid'),
                'tags' => '0',
                'dataFlag' => 'item',
                'dataId' => [
                    'company_id' => $companyid,
                    'site_id' => $siteid,
                    'plant_id' => $plantid,
                    'asset_id' => $assetid,
                    'itemtype_id' => $itemtypeid,
                    'item_id' => $item->id
                ]
            );

            array_push($trees, $setObject);
        }

        return $trees;
    }
}
