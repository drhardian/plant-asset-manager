<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    protected $maxAttempts = 3;

    public function index() {
        $attributes = [
            'title' => 'PAM - Plant Asset Management'
        ];

        return view('auth.login', [
            'pageAttributes' => $attributes
        ]);
    }

    public function authenticate(AuthRequest $request) {
        $this->checkTooManyAttempts();

        if(Auth::attempt(['username' => $request->username, 'password' => $request->password, 'user_status' => True], $request->rememberCheck)) {
            RateLimiter::clear($this->throttleKey());
            
            $request->session()->regenerate();

            return redirect()->intended('home');
        }

        RateLimiter::hit($this->throttleKey(), 60);

        return back()->withErrors([
            'errors' => __('auth.failed'),
            'attempts' => 'Remaining '.(intval(RateLimiter::remaining($this->throttleKey(), $this->maxAttempts))+1).' login attempts'
        ])->withInput();
    }

    public function throttleKey()
    {
        return Str::lower(request('username')).'|'.request()->ip();
    }

    public function checkTooManyAttempts()
    {
        if(RateLimiter::tooManyAttempts($this->throttleKey(), $this->maxAttempts)) {
            $availableIn = RateLimiter::availableIn($this->throttleKey());
            abort(429, ($availableIn));
        }

        return;
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerate();
        // $request->session()->regenerateToken();
        
        return redirect('login');
    }

    public function authrecovery(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        $checkemail = User::where('email', $request->email)->count();

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->errors()
            ]);
        } else {
            if($checkemail > 0) {
                return response()->json([
                    'status' => 200,
                    'errors' => 'Password reset email sent'
                ]);
            }

            return response()->json([
                'status' => 400,
                'errors' => [
                    'email' => 'Invalid email address'
                ]
            ]);
        }

    }
}
