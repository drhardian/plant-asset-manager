<?php

namespace App\Http\Traits;
use Illuminate\Support\Facades\Schema;

trait SubitemTrait {
    public function showItemHeaders()
    {
        return array('','Kolom 1','Kolom 2','Kolom 3');
    }

    public function getItemColumn()
    {
        $itemColumns = collect(Schema::getColumnListing('items')); #Get column name from items table
        $filterItemColumn = $itemColumns->only(0,1,2,3); #Filter specific column to show
        
        return $filterItemColumn->all(); #Show columns after filter process
    }

    public function showItemColumns()
    {
        $columns = $this->getItemColumn();

        $rows = [];

        foreach ($columns as $column) {
            $rows[] = array(
                'data' => ($column === "id") ? 'action' : $column,
                'name' => ($column === "id") ? 'action' : $column,
                'className' => 'text-center',
                'width' => ($column === "id") ? '10%' : '',
                'orderable' => ($column === "id") ? false : true,
                'searchable' => ($column === "id") ? false : true
            );
        }

        return $rows;
    }
}