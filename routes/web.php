<?php

use App\Http\Controllers\AssetController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ItemtypeController;
use App\Http\Controllers\PlantController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\SubitemController;
use App\Http\Controllers\TreecontentController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index_wellcome'])->name('wellcome');
Route::get('login', [AuthController::class, 'index'])->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
Route::post('login/process', [AuthController::class, 'authenticate'])->name('authprocess');
Route::middleware('auth')->group(function () {
    Route::get('home', [HomeController::class, 'index'])->name('home');
    Route::prefix('treemenu')->controller(TreecontentController::class)->group(function () {
        Route::get('/', 'show')->name('treemenus');
    });

    Route::prefix('company')->controller(CompanyController::class)->group(function () {
        Route::get('/', 'showCompanies')->name('companylist');
        Route::get('/show', 'show')->name('companydetails');
    });
    
    Route::prefix('site')->controller(SiteController::class)->group(function () {
        Route::get('/', 'showSites')->name('sitelist');
        Route::get('/show', 'show')->name('sitedetails');
    });
    
    Route::prefix('plant')->controller(PlantController::class)->group(function () {
        Route::get('/', 'showPlants')->name('plantlist');
        Route::get('/show', 'show')->name('plantdetails');
    });

    Route::prefix('asset')->controller(AssetController::class)->group(function () {
        Route::get('show', 'showAssets')->name('assetlist');
    });

    Route::prefix('itemtype')->controller(ItemtypeController::class)->group(function () {
        Route::get('show', 'showItemtypes')->name('itemtypelist');
    });

    Route::prefix('item')->controller(ItemController::class)->group(function () {
        Route::get('show/grid', 'showItemsOnGrid')->name('showitemsongrid');
        Route::get('show/sub/grid', 'showSubItemsOnGrid')->name('showsubitemsongrid');
        Route::get('show/selectbox', 'showItemsOnSelectbox')->name('showitemsonselectbox');
        Route::get('show/selectbox/selected', 'showSelectedItemOnSelectbox')->name('showselecteditemonselectbox');
        Route::get('show/details', 'show')->name('details_item');
    });

    Route::prefix('subitem')->controller(SubitemController::class)->group(function () {
        Route::get('show/grid', 'showSubItemsOnGrid')->name('showsubitemsongrid');
    });
});
