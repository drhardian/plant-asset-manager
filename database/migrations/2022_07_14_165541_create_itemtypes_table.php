<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itemtypes', function (Blueprint $table) {
            $table->id();
            $table->string('description', 150);
            $table->timestamps();
        });

        Schema::table('itemtypes', function (Blueprint $table) {
            $table->foreignId('company_id')->constrained()->after('id');
            $table->foreignId('site_id')->constrained()->after('company_id');
            $table->foreignId('plant_id')->constrained()->after('site_id');
            $table->foreignId('asset_id')->constrained()->after('plant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itemtypes');
    }
}
