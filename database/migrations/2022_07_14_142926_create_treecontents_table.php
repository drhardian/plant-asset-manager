<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTreecontentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treecontents', function (Blueprint $table) {
            $table->id();
            $table->string('displaytext', 50);
            $table->string('directlink', 100)->nullable()->default('#');
            $table->string('tags', 5)->default('0');
            $table->string('nodeid', 20);
            $table->tinyInteger('nodelevel');
            $table->string('nodeparent', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treecontents');
    }
}
