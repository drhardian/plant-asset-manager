<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\Company;
use App\Models\Plant;
use App\Models\Site;
use Illuminate\Database\Seeder;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::where('description', 'Perusahaan 1')->first();
        $site = Site::where('description', 'Site 1')->first();
        $plant = Plant::where('description', 'Plant 1')->first();

        Asset::insert([
            [ 
                'description' => 'Asset 1',
                'company_id' => $company->id,
                'site_id' => $site->id,
                'plant_id' => $plant->id
            ],
            [ 
                'description' => 'Asset 2',
                'company_id' => $company->id,
                'site_id' => $site->id,
                'plant_id' => $plant->id
            ]
        ]);
    }
}
