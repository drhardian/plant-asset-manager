<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\Company;
use App\Models\Item;
use App\Models\Itemtype;
use App\Models\Plant;
use App\Models\Site;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::where('description', 'Perusahaan 1')->first();
        $site = Site::where('description', 'Site 1')->first();
        $plant = Plant::where('description', 'Plant 1')->first();
        $asset = Asset::where('description', 'Asset 1')->first();
        $itemtype1 = Itemtype::where('description', 'Type 1')->first();
        $itemtype2 = Itemtype::where('description', 'Type 2')->first();

        Item::insert([
            [ 
                'field_1' => 'Isi Field 1.1',
                'field_2' => 'Isi Field 1.2',
                'field_3' => 'Isi Field 1.3',
                'company_id' => $company->id,
                'site_id' => $site->id,
                'plant_id' => $plant->id,
                'asset_id' => $asset->id,
                'itemtype_id' => $itemtype1->id
            ],
            [ 
                'field_1' => 'Isi Field 2.1',
                'field_2' => 'Isi Field 2.2',
                'field_3' => 'Isi Field 2.3',
                'company_id' => $company->id,
                'site_id' => $site->id,
                'plant_id' => $plant->id,
                'asset_id' => $asset->id,
                'itemtype_id' => $itemtype1->id
            ],
            [ 
                'field_1' => 'Isi Field 3.1',
                'field_2' => 'Isi Field 3.2',
                'field_3' => 'Isi Field 3.3',
                'company_id' => $company->id,
                'site_id' => $site->id,
                'plant_id' => $plant->id,
                'asset_id' => $asset->id,
                'itemtype_id' => $itemtype1->id
            ],
            [ 
                'field_1' => 'Isi Field 4.1',
                'field_2' => 'Isi Field 4.2',
                'field_3' => 'Isi Field 4.3',
                'company_id' => $company->id,
                'site_id' => $site->id,
                'plant_id' => $plant->id,
                'asset_id' => $asset->id,
                'itemtype_id' => $itemtype1->id
            ],
            [ 
                'field_1' => 'Isi Field 1.1',
                'field_2' => 'Isi Field 1.2',
                'field_3' => 'Isi Field 1.3',
                'company_id' => $company->id,
                'site_id' => $site->id,
                'plant_id' => $plant->id,
                'asset_id' => $asset->id,
                'itemtype_id' => $itemtype2->id
            ],
            [ 
                'field_1' => 'Isi Field 2.1',
                'field_2' => 'Isi Field 2.2',
                'field_3' => 'Isi Field 2.3',
                'company_id' => $company->id,
                'site_id' => $site->id,
                'plant_id' => $plant->id,
                'asset_id' => $asset->id,
                'itemtype_id' => $itemtype2->id
            ]
        ]);
    }
}
