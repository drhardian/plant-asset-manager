<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Site;
use Illuminate\Database\Seeder;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::where('description', 'Perusahaan 1')->first();

        Site::insert([
            [ 
                'description' => 'Site 1',
                'company_id' => $company->id
            ],
            [ 
                'description' => 'Site 2',
                'company_id' => $company->id
            ],
            [ 
                'description' => 'Site 3',
                'company_id' => $company->id
            ],
        ]);
    }
}
