<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrator',
            'username' => 'administrator',
            'role_id' => 0,
            'email' => 'administrator.pam@ptcs.com',
            'email_verified_at' => now(),
            'password' => Hash::make('1234567890'),
            'user_status' => True
        ]);
    }
}
