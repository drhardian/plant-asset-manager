<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Plant;
use App\Models\Site;
use Illuminate\Database\Seeder;

class PlantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::where('description', 'Perusahaan 1')->first();
        $site = Site::where('description', 'Site 1')->first();

        Plant::insert([
            [ 
                'description' => 'Plant 1',
                'company_id' => $company->id,
                'site_id' => $site->id
            ],
            [ 
                'description' => 'Plant 2',
                'company_id' => $company->id,
                'site_id' => $site->id
            ],
        ]);
    }
}
